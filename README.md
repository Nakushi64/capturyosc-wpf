![alt text](Logo/CapturyOSC26.png)

# CapturyOSC 2.0 - Ein WPF-Tool zum Senden und Empfangen von OSC-Daten für Captury


###### Quickstart Empfangen
Um das Tool nutzen zu können muss die .exe Datei aus dem bin/Release Ordner gestartet werden. Zu Testzwecken befindet sich die alte Version noch im bin/Debug Ordner. Die .exe und SharpOSC.dll müssen sich im selben Ordner befinden und können an jeden beliebigen Ort kopiert werden.
Nach dem Start muss eine IP-Adresse und Port von Captury angegeben werden, an der die Subscribe-Messages mit den ausgewählten Joints geschickt werden soll.
Dann muss man die IP-Adresse und Port zum Empfangen der Daten von OSC angeben. (Die IP des Computers an dem CapturyOSC WPF gestartet wurde und der Port über den die OSC-Daten empfangen werden sollen) In dieser Version (2.0) wird eine lokale IP beim Öffnen der Anwendung automatisch erkannt und eingetragen. 
Es sollte ein Dateiname festgelegt werden und optional kann ein Ordner zur Speicherung der OSC-Datei angegeben werden. (Standardmäßig wird die Datei in %APPDATA%/CapturyOSC gespeichert)
Sind alle Felder ausgefüllt, muss nur noch Start geklickt werden und es werden Subscribe-Nachrichten an Captury geschickt und Captury sendet die OSC-Daten für die einzelnen Joints zurück.
In der Konsole sieht man die Daten welches das Tool empfängt.
Oben rechts am Fenster befinden sich zusätzlich zwei Indikatoren, die jeweils den Zustand des Empfangens und Sendens anzeigen. Grün gibt dabei an, dass Daten erfolgreich empfangen oder gesendet werden. 
Das Programm schreibt alle empfangenen Daten in eine .osc Datei und speichert diese ab.
Mit dem Stop-Knopf beendet man das Empfangen der Daten und diese werden in die Datei geschrieben.
Möchte man von beliebiger Quelle OSC-Daten empfangen, so trägt man in das Textfeld für die Captury IP einfach eine 0 ein. Dies ermöglicht auch eine Loopback-Übertragung.

###### Quickstart Senden
Damit man die empfangenen Daten von Captury an VizArtist oder ähnliches schicken kann, muss nur die IP und der Port des Gerätes angegeben werden, welches die OSC-Daten empfangen soll.
Danach muss nur noch die Datei ausgewählt werden, welche gesendet werden soll.
Anschließend klickt man auf Start und die Daten werden gesendet.
Mit dem Stop-Knopf beendet man das Senden der Daten.

##2.0
- UI überarbeitet und intuitiver gestaltet.
- Indikatoren hinzugefügt, die anzeigen, ob empfangen oder gesendet wird.
- Back-End Fehler behoben und mehr Stabilität hinzugefügt.
- Port-Checks eingefügt, um sicherzustellen das auf den jeweiligen Ports empfangen und gesendet werden kann.
- Ping eingefügt, um sicherzustellen, dass eine Verbindung zu Captury besteht.
- Automatische Erkennung der lokalen IP.
- Konsolenausgaben für eine leichtere Fehlerbehebung.
- Empfang von OSC-Daten aus beliebiger Quelle (auch Loopback), wenn man 0 für die Captury IP eingibt.
- Icon für die .exe designt.
<hr/>
![alt text](Logo/ScreenshotEmpfang.png)
![alt text](Logo/ScreenshotSenden.png)


