﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Timers;
using SharpOSC;
using WinForms = System.Windows.Forms;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CapturyOSCWPF
{
    public partial class MainWindow : Window
    {
        static string directoryPath;
        static IPEndPoint remoteEP;
        static UdpClient udpClient, sendUdpClient;
        static string fileName;
        static StreamWriter streamWriter;
        static Boolean killUDPCallback;
        static Timer timer, timerIndicator;
        static String sPath;
        static String sFile;
        static Boolean started = false;
        static bool sendData;
        static int peopleCounter;
        static string[][] subscribeMessageArray;
        static string[] jointNames;
        static Stopwatch swTimestamp, stopWatchRunIndicator;
        static bool receiveAnyChecked = false;

        public MainWindow()
        {
            InitializeComponent();
            peopleCounter = 1; 
            //Recognize local IP:
            string ipadress = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString();
            ipReceiveData.Text = ipadress;
            //
        }

        private bool checkPort(string portNo)
        {
            int myport = Int32.Parse(portNo);
            if (System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().GetActiveUdpListeners().Any(p => p.Port == myport))
            {
                WriteLine("Port " + myport + " wird bereits verwendet! Port freigeben oder einen anderen Port verwenden!");
                WriteLine("Nächster freier Port: " + Enumerable.Range(1066, 1099).First(p => !IPGlobalProperties.GetIPGlobalProperties().GetActiveUdpListeners().Any(l => l.Port == p)));
                return false;
            }
            return true;
        }

        private void startBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!started)
            {
                if (!checkPort(portReceiveData.Text))
                {
                    started = false;
                    if(udpClient != null) udpClient.Close();
                    return;
                }

                swTimestamp = new Stopwatch();
                stopWatchRunIndicator = new Stopwatch();
                stopWatchRunIndicator.Start();
                subscribeMessageArray = new string[peopleCounter][];
                killUDPCallback = false;
                string ipSubMessage = this.ipSubMessage.Text;
                int portSubMessage = Int32.Parse(this.portSubMessage.Text);
                if (!receiveAnyChecked && (ipSubMessage != "0")) remoteEP = new IPEndPoint(IPAddress.Parse(ipSubMessage), portSubMessage);

                if (!String.IsNullOrEmpty(sPath))
                {
                    directoryPath = sPath;
                }
                else
                {
                    directoryPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CapturyOSC");
                }

                fileName = this.oscFileName.Text + ".osc";

                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                streamWriter = File.CreateText(System.IO.Path.Combine(directoryPath, fileName));

                //init sender
                try
                {
                    string ipSender = this.ipReceiveData.Text;
                    int portSender = Int32.Parse(this.portReceiveData.Text);
                    
                    try
                    {
                        udpClient = new UdpClient();
                        //Allow loopback:
                        udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                        //
                        udpClient.Client.Bind(new IPEndPoint(IPAddress.Parse(ipSender), portSender));
                        if(!receiveAnyChecked && (ipSubMessage != "0")) udpClient.Connect(remoteEP);
                    }
                    catch (Exception ex)
                    {
                        WriteLine(ex.ToString()+"\nIP ungültig.");
                        killUDPCallback = true;
                        started = false;
                        iconRunning.Fill = System.Windows.Media.Brushes.Red;

                        startBtn.Visibility = Visibility.Visible;
                        stopBtn.Visibility = Visibility.Collapsed;
                        if (udpClient != null && timer != null)
                        {
                            udpClient.Close();
                            timer.Stop();
                        }

                        streamWriter.Close();
                        swTimestamp.Stop();
                        return;
                    }

                    //send starting message
                    jointNames = subscribeContent.Text.Split(',');
                    string[] subscribe;

                    for(int i = 1; i <= peopleCounter; i++)
                    {
                        if(i == 1)
                        {
                            subscribe = subscribeMessage.Text.Split('+');
                            subscribeMessageArray[0] = subscribe;
                            
                        }
                        else
                        {
                            TextBox tmp = (TextBox)this.subscribeMessageGrid.FindName("subscribeMessage" + i);
                            subscribe = tmp.Text.Split('+');
                            subscribeMessageArray[i-1] = subscribe;
                        }
                        foreach (string joint in jointNames)
                        {
                            var message = new OscMessage(subscribe[0] + joint + subscribe[2]);
                            if (!receiveAnyChecked && (ipSubMessage != "0")) udpClient.Send(message.GetBytes(), message.GetBytes().Length);
                        }
                    }

                    if (!receiveAnyChecked && (ipSubMessage != "0"))
                    {
                        WriteLine("Subscribe-Nachricht gesendet an: " + ipSubMessage + " : " + portSubMessage);

                        //Ping Remote
                        Ping pingSender = new Ping();
                        PingOptions options = new PingOptions();
                        options.DontFragment = true;
                        string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                        byte[] buffer = Encoding.ASCII.GetBytes(data);
                        int timeout = 120;
                        PingReply reply = pingSender.Send(ipSubMessage, timeout, buffer, options);
                        if (reply.Status == IPStatus.Success)
                        {
                            WriteLine("Round Trip time: " + reply.RoundtripTime);
                            WriteLine("Ping erfolgreich!");
                        }
                        else
                        {
                            WriteLine("Ping fehlgeschlagen! Captury-IP überprüfen!");
                        }
                    }

                    //Receive Messages
                    try 
                    { 
                        udpClient.BeginReceive(new AsyncCallback(callback), udpClient);
                        WriteLine("Erwarte Nachricht auf: " + ipSender + " : " + portSender);
                    }
                    catch(Exception ex)
                    {
                        WriteLine(ex.ToString());
                    }
                    
                    //setup timer and subscription messages
                    timer = new Timer();
                    timer.Interval = Int32.Parse(this.timerInterval.Text);
                    timer.Enabled = true;

                    Stopwatch sw = Stopwatch.StartNew();
                    long start = 0;
                    long end = sw.ElapsedMilliseconds;

                    timerIndicator = new Timer();
                    timerIndicator.Interval = 250;
                    timerIndicator.Enabled = true;

                    if (killUDPCallback == false)
                    {
                        timer.Elapsed += (o, E) =>
                        {
                            start = end;
                            end = sw.ElapsedMilliseconds;
                            for (int i = 0; i < peopleCounter; i++)
                            {
                                subscribe = subscribeMessageArray[i];
                                foreach (string joint in jointNames)
                                {
                                    var message = new OscMessage(subscribe[0] + joint + subscribe[2]);
                                    if (!receiveAnyChecked && (ipSubMessage != "0")) udpClient.Send(message.GetBytes(), message.GetBytes().Length);
                                }
                            }
                            WriteLine("Subscribe-Nachricht erneut versendet.");
                        };
                    }

                    timerIndicator.Elapsed += (o, E) =>
                    {
                        if (killUDPCallback == false)
                        {
                            if(stopWatchRunIndicator.ElapsedMilliseconds > 500) Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() => { iconRunning.Fill = System.Windows.Media.Brushes.Yellow; stopBtn.Background = System.Windows.Media.Brushes.Orange; }));
                        }
                        else
                        {
                            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() => iconRunning.Fill = System.Windows.Media.Brushes.Red));
                        }
                    };

                    started = true;
                    startBtn.Visibility = Visibility.Collapsed;
                    stopBtn.Visibility = Visibility.Visible;
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() => { iconRunning.Fill = System.Windows.Media.Brushes.Yellow; stopBtn.Background = System.Windows.Media.Brushes.Orange; }));

                }
                catch (Exception E)
                {
                    WriteLine(E.Message);
                    started = false;
                    iconRunning.Fill = System.Windows.Media.Brushes.Red;
                    startBtn.Visibility = Visibility.Visible;
                    stopBtn.Visibility = Visibility.Collapsed;
                    streamWriter.Close();
                }
            }
            else
            {
                WriteLine("Zuerst die Anwendung stoppen!");
            }
        }

        private void ReadAndWriteOscMessage(StreamWriter streamWriter, OscMessage receivedPacket)
        {
            if(!swTimestamp.IsRunning)
            {
                swTimestamp = Stopwatch.StartNew();
            }
            streamWriter.Write((int) swTimestamp.Elapsed.TotalMilliseconds + ";");
            streamWriter.Write(receivedPacket.Address);
            foreach (object argument in receivedPacket.Arguments)
            {
                streamWriter.Write(";");
                streamWriter.Write(argument.ToString() + "|f");
            }
            streamWriter.Write("\n");
        }

        private void oscMessageConsole(OscMessage receivedPacket)
        {
            String receivedLine = "";
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += delegate (object sender, DoWorkEventArgs e) {
                receivedLine = receivedPacket.Address;
                foreach (object argument in receivedPacket.Arguments)
                {;
                    receivedLine += argument.ToString();
                }
                (sender as BackgroundWorker).ReportProgress(1, receivedLine);
            };
            worker.ProgressChanged += delegate (object sender, ProgressChangedEventArgs e) {
                WriteLine((String)e.UserState);
            };
            worker.RunWorkerAsync(10000);
        }

        //Called when message being received
        private void callback(IAsyncResult ar)
        {
            try
            {
                if (killUDPCallback == false)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() => { iconRunning.Fill = System.Windows.Media.Brushes.Green; stopBtn.Background = System.Windows.Media.Brushes.Green; }));
                    stopWatchRunIndicator.Restart();
                    UdpClient udpClient = ((UdpClient)(ar.AsyncState));
                    byte[] receiveBytes = udpClient.EndReceive(ar, ref remoteEP);
                    string receiveString = Encoding.ASCII.GetString(receiveBytes);
                    OscPacket packet = OscPacket.GetPacket(receiveBytes);
                    OscBundle receivedBundle = packet as OscBundle;

                    if (receivedBundle == null)
                    {
                        OscMessage receivedMessage = packet as OscMessage;
                        ReadAndWriteOscMessage(streamWriter, receivedMessage);
                        if (OSCDatenAnzeigen.Dispatcher.Invoke(() => OSCDatenAnzeigen.IsChecked) ?? false) oscMessageConsole(receivedMessage);
                    }
                    else
                    {
                        foreach (OscMessage receivedMessage in receivedBundle.Messages)
                        {
                            ReadAndWriteOscMessage(streamWriter, receivedMessage);
                            if (OSCDatenAnzeigen.Dispatcher.Invoke(() => OSCDatenAnzeigen.IsChecked) ?? false) oscMessageConsole(receivedMessage);
                        }
                    }
                    if(udpClient.Client != null) udpClient.BeginReceive(new AsyncCallback(callback), udpClient);
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() => iconRunning.Fill = System.Windows.Media.Brushes.Red));
                }

            }
            catch (Exception ex)
            {
                WriteLine(ex.ToString());
            }

        }

        private void stopBtn_Click(object sender, RoutedEventArgs e)
        {
            if (started)
            {
                killUDPCallback = true;
                started = false;
                startBtn.Visibility = Visibility.Visible;
                stopBtn.Visibility = Visibility.Collapsed;
                
                if (udpClient != null) udpClient.Close();
                if(timer != null) timer.Stop();
                streamWriter.Close();
                stopWatchRunIndicator.Stop();
                swTimestamp.Stop();
                WriteLine("Empfangvorgang angehalten.");
            }
            else
            {
                WriteLine("Anwendung ist bereits angehalten!");
            }
        }

        private void pathButton_Click(object sender, RoutedEventArgs e)
        {
            WinForms.FolderBrowserDialog folderDialog = new WinForms.FolderBrowserDialog();
            folderDialog.ShowNewFolderButton = false;
            folderDialog.SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory;
            WinForms.DialogResult result = folderDialog.ShowDialog();

            if (result == WinForms.DialogResult.OK)
            {
                sPath = folderDialog.SelectedPath;
                TextBlock tb = this.selectedFolderInfo;
                tb.Text = "";
                tb.Inlines.Add("Ausgewählter Ordner: ");
                tb.Inlines.Add(new Run(sPath) { FontWeight = FontWeights.Bold });
            }
        }

        //Send methods
        private void startBtnSend_Click(object sender, RoutedEventArgs e)
        {
            if (!checkPort(portSubMessage.Text))
            {
                started = false;
                if (udpClient != null) udpClient.Close();
                return;
            }
            sendData = true;
            sendOscDataFromFile();
        }

        void sendOscDataFromFile()
        {
            Stopwatch sw = Stopwatch.StartNew();
            int people = Int32.Parse(peopleNumber.Text);
            double timing;

            timing = 1;
            try
            {
                if (!String.IsNullOrEmpty(sFile))
                {
                    string ipSender = this.ipRemote.Text;
                    int portSender = Int32.Parse(this.portRemote.Text);
                    IPEndPoint sendRemoteEP = new IPEndPoint(IPAddress.Parse(ipSender), portSender);
                    sendUdpClient = new UdpClient(8888);

                    sendUdpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

                    try
                    {
                        sendUdpClient.Connect(sendRemoteEP);
                    }
                    catch (Exception ex)
                    {
                        WriteLine(ex.ToString());
                    }

                    //Put into UI thread if timing problems occur:
                    startBtnSend.Visibility = Visibility.Collapsed;
                    stopBtnSend.Visibility = Visibility.Visible;
                    iconSending.Fill = System.Windows.Media.Brushes.Green;
                    //
                    BackgroundWorker worker = new BackgroundWorker();
                    worker.WorkerReportsProgress = true;

                    WriteLine("Sende Daten an: " + ipSender + " : " + portSender);

                    worker.DoWork += delegate (object sender, DoWorkEventArgs e)
                    {
                        //send starting message
                        while (sendData == true)
                        {

                            using (StreamReader sr = new StreamReader(sFile))
                            {
                                string line = sr.ReadLine();

                                while (line != null && sendData == true)
                                {

                                    if ((sw.Elapsed.TotalMilliseconds) >= timing)
                                    {
                                        object[] messageArray = line.Split(';');
                                        double currentTime = Double.Parse(messageArray[0].ToString());
                                        string oscAdress = (string)messageArray[1];
                                        object[] convertedMessage = new object[messageArray.Length - 2];

                                        for (int i = 2; i < messageArray.Length; i++)
                                        {
                                            convertedMessage[i - 2] = float.Parse(messageArray[i].ToString().Replace("|f", "").Replace(",", "."), CultureInfo.InvariantCulture);
                                        }
                                        var message = new OscMessage(oscAdress, convertedMessage);
                                        sendUdpClient.Send(message.GetBytes(), message.GetBytes().Length);
                                        line = sr.ReadLine();
                                        if(line != null)
                                        {
                                            object[] nextMessageArray = line.Split(';');
                                            double nextTime = Double.Parse(nextMessageArray[0].ToString());
                                            timing = nextTime - currentTime;
                                        }
                                       
                                        sw = Stopwatch.StartNew();
                                    }
                                }
                                
                            }                        
                        }
                    };

                    worker.RunWorkerCompleted += delegate (object sender, RunWorkerCompletedEventArgs e)
                    {
                        sendUdpClient.Close();
                        WriteLine("Senden beendet.");
                        iconSending.Fill = System.Windows.Media.Brushes.Red;
                        startBtnSend.Visibility = Visibility.Visible;
                        stopBtnSend.Visibility = Visibility.Collapsed;
                    };
                    worker.RunWorkerAsync(10);
                }

                else
                {
                    WriteLine("Keine Datei ausgewählt");
                }
            }
            catch(Exception e)
            {
                WriteLine(e.Message);
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            WriteLine((string) e.UserState);
        }

        private void stopBtnSend_Click(object sender, RoutedEventArgs e)
        {
            iconSending.Fill = System.Windows.Media.Brushes.Red;
            startBtnSend.Visibility = Visibility.Visible;
            stopBtnSend.Visibility = Visibility.Collapsed;
            sendData = false;
        }

        private void fileButton_Click(object sender, RoutedEventArgs e)
        {
            WinForms.OpenFileDialog fileDialog = new WinForms.OpenFileDialog();
            WinForms.DialogResult result = fileDialog.ShowDialog();

            if (result == WinForms.DialogResult.OK)
            {
                sFile = fileDialog.FileName;
                TextBlock tb = this.selectedFileInfo;
                tb.Text = "";
                tb.Inlines.Add("Ausgewählte Datei: ");
                tb.Inlines.Add(new Run(sFile) { FontWeight = FontWeights.Bold });
            }
        }

       private void WriteLine(String line)
        {
            if(consoleBox.Visibility == Visibility.Visible)
            {
                // Old: Dispatcher.BeginInvoke((Action)(() =>  consoleBox.AppendText(line + "\n") )); consoleBox.ScrollToEnd();
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(() => { consoleBox.AppendText(line + "\n"); consoleBox.ScrollToEnd(); }));


            }
        }

        //Events for number of tracked persons
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int tmp = Int32.Parse(peopleNumber.Text)+1;
            if(tmp <= 3)
            {
                TextBox txtNumber = new TextBox();
                txtNumber.Name = "subscribeMessage" + tmp;
                txtNumber.Text = "/subscribe/unknown-" + tmp + "/maya/+ joint +/matrix";
                txtNumber.FontSize = 20;
                txtNumber.SetValue(Grid.RowProperty, tmp - 1);
                Thickness myThickness = new Thickness();
                myThickness.Bottom = 5;
                txtNumber.Margin = myThickness;
                subscribeMessageGrid.Children.Add(txtNumber);
                subscribeMessageGrid.RegisterName(txtNumber.Name, txtNumber);
                peopleCounter = tmp;
                peopleNumber.Text = Convert.ToString(Int32.Parse(peopleNumber.Text) + 1);
            }
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            int tmp = Int32.Parse(peopleNumber.Text);
            if(tmp > 1)
            {
                TextBox tb = (TextBox)this.subscribeMessageGrid.FindName("subscribeMessage" + tmp);
                subscribeMessageGrid.Children.Remove(tb);
                subscribeMessageGrid.UnregisterName("subscribeMessage" + tmp);
                peopleNumber.Text = Convert.ToString(tmp - 1);
            }
        }

        private void ReceiveAny_Checked(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(() => {
                receiverText.Visibility = Visibility.Collapsed;
                receiverUDPText.Visibility = Visibility.Collapsed;
                ipSubMessage.Visibility = Visibility.Collapsed;
                portSubMessage.Visibility = Visibility.Collapsed;
                expander.IsEnabled = false;
                foreach(TextBlock tb in expander.Children.OfType<TextBlock>())
                {
                    tb.Foreground = System.Windows.Media.Brushes.Gray;
                }
                foreach (Grid g in expander.Children.OfType<Grid>())
                {
                    foreach(TextBlock tb in g.Children.OfType<TextBlock>())
                    {
                        tb.Foreground = System.Windows.Media.Brushes.Gray;
                    }
                }
                senderPortText.Margin = new Thickness(5, 5, 40, 5);
                senderPortText.Text = "Lokaler Port";
            }));
            receiveAnyChecked = true;
        }
        private void ReceiveAny_UnChecked(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(() => {
                receiverText.Visibility = Visibility.Visible;
                receiverUDPText.Visibility = Visibility.Visible;
                ipSubMessage.Visibility = Visibility.Visible;
                portSubMessage.Visibility = Visibility.Visible;
                expander.IsEnabled = true; 
                foreach (TextBlock tb in expander.Children.OfType<TextBlock>())
                {
                    tb.Foreground = (System.Windows.Media.Brush)Application.Current.Resources["ControlDefaultForeground"];
                }
                foreach (Grid g in expander.Children.OfType<Grid>())
                {
                    foreach (TextBlock tb in g.Children.OfType<TextBlock>())
                    {
                        tb.Foreground = (System.Windows.Media.Brush)Application.Current.Resources["ControlDefaultForeground"];
                    }
                }
                senderPortText.Margin = new Thickness(5,5,5,5);
                senderPortText.Text = "Captury UDP Reply Port";
            }));
            receiveAnyChecked = false;
        }
    }
}
//udpClient == null, if udpClient exists
//udpClient.Client == null, if udClient is connected
//Use custom colors like this: (System.Windows.Media.Brush)Application.Current.Resources["ControlDefaultForeground"]